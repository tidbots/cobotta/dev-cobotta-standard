# dev-cobotta-standard
Cobottaを動かす

https://wiki.ros.org/denso_robot_ros
https://wiki.ros.org/denso_robot_ros/Tutorials


# COBOTTA
(If you want to use COBOTTA, could you create a robot definition file, cobotta_bringup.launch. see the ROSConverter page.)

```
roslaunch denso_robot_bringup cobotta_bringup.launch sim:=false ip_address:=192.168.0.1 send_format:=0 recv_format:=2
```

# Simulation

```
roslaunch denso_robot_bringup vs060_bringup.launch sim:=false ip_address:=192.168.0.1
```

